# Multi-project using config file

This example shows how to run Infracost in GitLab CI with multiple Terraform projects using a config file. The [config file]((https://www.infracost.io/docs/config_file/)) can be used to specify variable files or the Terraform workspaces for different projects.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: multi-project-config-file)
```yml
variables:
  CONFIG_FILE:  examples/multi-project-config-file/code/infracost.yml
stages:
  - infracost

infracost:
  stage: infracost
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  script:
    # If you use private modules, add an environment variable or secret
    # called GIT_SSH_KEY with your private key, so Infracost can access
    # private repositories (similar to how Terraform/Terragrunt does).
    # - mkdir -p ~/.ssh
    # - eval `ssh-agent -s`
    # - echo "$GIT_SSH_KEY" | tr -d '\r' | ssh-add -
    # Update this to github.com, gitlab.com, bitbucket.org, ssh.dev.azure.com or your source control server's domain
    # - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

    # Clone the base branch of the pull request (e.g. main/master) into a temp directory.
    - git clone $CI_REPOSITORY_URL --branch=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME --single-branch /tmp/base

    # Generate an Infracost cost snapshot from the comparison branch, so that Infracost can compare the cost difference.
    - |
      cd /tmp/base
      infracost breakdown --config-file=${CONFIG_FILE} \
                          --format=json \
                          --out-file=infracost-base.json

    # Generate an Infracost diff and save it to a JSON file.
    - |
      cd  -
      infracost diff --config-file=${CONFIG_FILE} \
                     --compare-to=/tmp/base/infracost-base.json \
                     --format=json \
                     --out-file=infracost.json

    # See https://gitlab.com/infracost/infracost-gitlab-ci/#comment-options for other options.
    # Choose the commenting behavior, 'update' is a good default:
    #   update: Create a single comment and update it. The "quietest" option.
    #   delete-and-new: Delete previous comments and create a new one.
    #   new: Create a new cost estimate comment on every push.
    - |
      infracost comment gitlab --path=infracost.json \
                               --repo=$CI_PROJECT_PATH \
                               --merge-request=$CI_MERGE_REQUEST_IID \
                               --gitlab-server-url=$CI_SERVER_URL \
                               --gitlab-token=$GITLAB_TOKEN \
                               --behavior=update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)

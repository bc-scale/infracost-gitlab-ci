#!/usr/bin/env node
const { spawn } = require('child_process');
const glob = require('glob');

const gitlabCiDir = '.gitlab-ci';

const args = process.argv.slice(2);
const update = args.length > 0 && args[0] === 'true';

const localSkipTests = [
  // This test is skipped locally since the local gitlab-ci-local tool doesn't yet support matrix builds
  // See https://github.com/firecow/gitlab-ci-local/issues/205
  `${gitlabCiDir}/examples/plan-json-multi-project-matrix.yml`,
  `${gitlabCiDir}/examples/plan-json-multi-workspace-matrix.yml`,
]

const commands = [];

glob.sync(`${gitlabCiDir}/examples/*.yml`, `${gitlabCiDir}/examples/plan-json/*.yml`).forEach((path) => {
  if (localSkipTests.includes(path)) {
    console.log('Skipping test when running locally:', path);
    return null;
  }

  let command = `gitlab-ci-local \
    --variable SKIP_LOCAL=true \
    --variable INFRACOST_API_KEY=$(infracost configure get api_key) \
    --volume $(pwd)/testdata:/gcl-builds/testdata \
    --file=${path}`

  if (update) {
    command += ' --variable UPDATE_GOLDEN_FILES=true';
  }

  commands.push(command);
});

function runCommand(command) {
  return new Promise((resolve, reject) => {
    const child = spawn('bash', ['-c', command], { env: process.env });

    child.stdout.on('data', (data) => {
      process.stdout.write(data.toString());
    });

    child.stderr.on('data', (data) => {
      process.stderr.write(data.toString());
    });

    child.on('exit', (code) => {
      if (code === 0) {
        resolve();
      } else {
        reject();
      }
    });
  });
}

async function runCommands() {
  for (const command of commands) {
    console.log(`Running ${command}`);

    await runCommand(command);
  }
}

runCommands().then(() => {
  console.log('DONE');
}).catch((err) => {
  if (err) {
    console.error(err);
  }
  console.log('FAILED');
});
